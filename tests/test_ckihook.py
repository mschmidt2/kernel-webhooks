"""Webhook interaction tests."""
from copy import deepcopy
from datetime import datetime
from unittest import TestCase
from unittest import mock

from tests import fake_payloads
from tests import fakes
from webhook import ckihook
from webhook import defs
from webhook.pipelines import PipelineType

CHANGES = {'labels': {'previous': [{'title': f'{defs.CKI_KERNEL_RT_PREFIX}::Running'},
                                   {'title': f'{defs.CKI_KERNEL_PREFIX}::Running'},
                                   {'title': f'{defs.CKI_KERNEL_AUTOMOTIVE_PREFIX}::Running'}
                                   ],
                      'current': [{'title': f'{defs.CKI_KERNEL_RT_PREFIX}::Canceled'}]
                      }
           }


class TestPipeStatus(TestCase):
    """Tests for the PipeStatus enum."""

    def test_title(self):
        """Title() should capitalize() all Statuses except OK."""
        self.assertEqual(ckihook.PipelineStatus.FAILED.title(), 'Failed')
        self.assertEqual(ckihook.PipelineStatus.CREATED.title(), 'Running')
        self.assertEqual(ckihook.PipelineStatus.OK.title(), 'OK')


class TestPipelineResult(TestCase):
    """Tests for the PipelineResult dataclass."""

    STAGES = [{'name': 'prepare', 'jobs': {'nodes': [{'status': 'SUCCESS'},
                                                     {'status': 'SUCCESS'}]}},
              {'name': 'build', 'jobs': {'nodes': [{'status': 'SUCCESS'},
                                                   {'status': 'SUCCESS'}]}},
              {'name': 'build-tools', 'jobs': {'nodes': [{'status': 'SUCCESS'},
                                                         {'status': 'RUNNING'}]}},
              {'name': 'test', 'jobs': {'nodes': [{'status': 'PENDING'},
                                                  {'status': 'PENDING'}]}},
              {'name': 'results', 'jobs': {'nodes': [{'status': 'PENDING'},
                                                     {'status': 'PENDING'}]}},
              ]

    def test_init_with_dict(self):
        """Object properties should be set."""
        stages = deepcopy(self.STAGES)
        # Sets label
        input_dict = {'id': 'gid://gitlab/Pipeline/123',
                      'createdAt': '2013-12-03T17:23:34Z',
                      'project': {'id': 'gid://gitlab/Project/456'},
                      'status': 'pending',
                      'sourceJob': {'name': 'rhel8_realtime_check_merge_request'},
                      'stages': {'nodes': stages}
                      }
        result = ckihook.PipelineResult(api_dict=input_dict)
        self.assertEqual(result.id, 123)
        self.assertEqual(result.project_id, 456)
        self.assertEqual(result.name, input_dict['sourceJob']['name'])
        self.assertEqual(result.status, ckihook.PipelineStatus.RUNNING)
        self.assertEqual(result.label, 'CKI_RT::Running')
        self.assertEqual(result.type, ckihook.PipelineType.REALTIME)
        # Sets failed label
        input_dict['status'] = 'failed'
        stages[-2]['jobs']['nodes'].append({'status': 'FAILED'})
        result = ckihook.PipelineResult(api_dict=input_dict)
        self.assertEqual(result.name, input_dict['sourceJob']['name'])
        self.assertEqual(result.status, ckihook.PipelineStatus.FAILED)
        self.assertEqual(result.label, 'CKI_RT::Failed::test')
        self.assertEqual(result.type, ckihook.PipelineType.REALTIME)
        # Sets no label
        input_dict['sourceJob']['name'] = 'downstream_work'
        result = ckihook.PipelineResult(api_dict=input_dict)
        self.assertEqual(result.name, input_dict['sourceJob']['name'])
        self.assertEqual(result.status, ckihook.PipelineStatus.FAILED)
        self.assertEqual(result.label, '')
        self.assertEqual(result.type, ckihook.PipelineType.INVALID)

    def test_get_failed_stage(self):
        """Returns a stage name if status is FAILED and a failed job is found, or None."""
        stages = deepcopy(self.STAGES)
        # No failed jobs
        self.assertEqual(ckihook.PipelineResult._get_failed_stage(stages), '')
        # Test job failed.
        stages[1]['jobs']['nodes'].append({'status': 'FAILED'})
        stages[-2]['jobs']['nodes'].append({'status': 'FAILED'})
        self.assertEqual(ckihook.PipelineResult._get_failed_stage(stages), 'test')


class TestHelpers(TestCase):
    """Test helper functions."""

    @mock.patch('webhook.ckihook.validate_cki_query_results')
    def test_fetch_cki(self, mock_validate):
        """Runs a query and returns the output of validate_pipeline_query_results()."""
        mock_graphql = mock.Mock()
        namespace = 'group/project'
        mr_id = 123
        mock_graphql.client.query.return_value = {'project': {}, 'currentUser': {'username': 'foo'}}
        result = ckihook.fetch_cki(mock_graphql, namespace, mr_id)
        self.assertEqual(result, mock_validate.return_value)

    def test_validate_cki_query_results(self):
        """Returns an empty list when query results do not have the necessary details."""
        # Nada.
        with self.assertLogs('cki.webhook.ckihook', level='INFO') as logs:
            self.assertEqual(ckihook.validate_cki_query_results([]), [])
            self.assertIn('Nothing to parse.', logs.output[-1])

        # No MR.
        with self.assertLogs('cki.webhook.ckihook', level='WARNING') as logs:
            self.assertEqual(ckihook.validate_cki_query_results({'project': {}}), [])
            self.assertIn('Merge request does not exist', logs.output[-1])

        # No head pipeline.
        with self.assertLogs('cki.webhook.ckihook', level='WARNING') as logs:
            graphql = {'project': {'mr': {'headPipeline': {}}}}
            self.assertEqual(ckihook.validate_cki_query_results(graphql), [])
            self.assertIn('MR does not have headPipeline set.', logs.output[-1])

        # No downstream notes.
        with self.assertLogs('cki.webhook.ckihook', level='WARNING') as logs:
            graphql = {'project': {'mr': {'headPipeline': {'downstream': {'nodes': []}}}}}
            self.assertEqual(ckihook.validate_cki_query_results(graphql), [])

    @mock.patch('webhook.ckihook.PipelineResult')
    def test_map_pipeline_query_results_good(self, mock_PipelineResult):
        """Passes downstream nodes to generate PipelineResult objects and returns them as a list."""
        pipe1 = mock.Mock(spec=['name', 'created_at'],
                          created_at=datetime.fromisoformat('2016-01-27'))
        pipe1.name = 'c9s_regular'
        pipe2 = mock.Mock(spec=['name', 'created_at'],
                          created_at=datetime.fromisoformat('2021-08-31'))
        pipe2.name = 'c9s_regular'
        pipe3 = mock.Mock(spec=['name', 'created_at'],
                          created_at=datetime.fromisoformat('2020-12-25'))
        pipe3.name = 'c9s_regular'
        mock_PipelineResult.side_effect = [pipe1, pipe2, pipe3]
        graphql = {'project': {'mr': {'headPipeline': {'downstream': {'nodes': [1, 2, 3]}}}}}
        results = ckihook.map_pipeline_query_results(graphql)
        self.assertEqual(mock_PipelineResult.call_count, 3)
        self.assertEqual(mock_PipelineResult.call_args_list[0].kwargs['api_dict'], 1)
        self.assertEqual(mock_PipelineResult.call_args_list[1].kwargs['api_dict'], 2)
        self.assertEqual(mock_PipelineResult.call_args_list[2].kwargs['api_dict'], 3)
        self.assertEqual(len(results), 1)
        self.assertEqual(results, [pipe2])

    @mock.patch('webhook.ckihook.get_instance')
    @mock.patch('webhook.common.add_label_to_merge_request')
    def test_add_labels(self, mock_add_label, mock_get_instance):
        """Gets a gl_instance/project and calls add_label_to_merge_request."""
        namespace = 'group/project'
        mr_id = 123
        labels = ['CKI::Running', 'CKI_RT::OK']
        ckihook.add_labels(namespace, mr_id, labels)
        mock_get_instance.projects.get.called_once_with(namespace)
        mock_add_label.called_once_with(mock_get_instance,
                                        mock_get_instance.projects.get.return_value, mr_id, labels,
                                        remove_scoped=True)

    def test_cki_label_changed(self):
        """Returns True if any CKI labels are in the changes list."""
        changes = deepcopy(CHANGES)
        self.assertTrue(ckihook.cki_label_changed(changes))
        self.assertFalse(ckihook.cki_label_changed({}))

    @mock.patch('webhook.ckihook.fetch_cki')
    @mock.patch('webhook.ckihook.map_pipeline_query_results')
    @mock.patch('webhook.ckihook.check_for_missing_labels')
    def test_compute_new_labels(self, mock_missing_check, mock_map_results, mock_fetch):
        """Returns a list of labels if any changed."""
        pipelines = [PipelineType.REALTIME, PipelineType.REGULAR, PipelineType.AUTOMOTIVE]
        mock_graphql = mock.Mock()
        namespace = 'group/project'
        mr_id = 123

        # CKI changed so we compute new labels.
        mock_map_results.return_value = [mock.Mock(label='CKI::OK', type=PipelineType.REGULAR),
                                         mock.Mock(label='CKI_RT::Failed::merge',
                                                   type=PipelineType.REALTIME),
                                         mock.Mock(label='CKI_Automotive::Failed::merge',
                                                   type=PipelineType.AUTOMOTIVE)]
        mock_missing_check.return_value = []
        results = ckihook.compute_new_labels(mock_graphql, namespace, mr_id, pipelines)
        mock_fetch.assert_called_with(mock_graphql, namespace, mr_id)
        mock_missing_check.assert_called_once_with(['CKI::OK',
                                                    'CKI_RT::Failed::merge',
                                                    'CKI_Automotive::Failed::merge'], pipelines)
        self.assertEqual(results, ['CKI::OK',
                                   'CKI_RT::Failed::merge',
                                   'CKI_Automotive::Failed::merge'])

    def test_check_for_missing_labels(self):
        """Returns a list with the ::Missing CKI labels."""
        pipelines = [PipelineType.REALTIME, PipelineType.REGULAR, PipelineType.AUTOMOTIVE]
        # Input list has neither, return all tags.
        missing_labels = {f'{defs.CKI_KERNEL_PREFIX}::Missing',
                          f'{defs.CKI_KERNEL_AUTOMOTIVE_PREFIX}::Missing',
                          f'{defs.CKI_KERNEL_RT_PREFIX}::Missing'}

        running_labels = []
        for i in range(len(missing_labels)):
            self.assertEqual(set(ckihook.check_for_missing_labels(running_labels, pipelines)),
                             missing_labels)

            # When all tests are running and missing labels is empty
            if missing_labels:
                running_labels.append(missing_labels.pop().replace("Missing",
                                                                   "Running"))

    @mock.patch('webhook.ckihook.fetch_cki')
    @mock.patch('webhook.ckihook.get_downstream_pipeline_branch')
    @mock.patch('webhook.common.cancel_pipeline')
    @mock.patch('webhook.common.create_mr_pipeline')
    def test_process_possible_branch_change_false(self, mock_create, mock_cancel,
                                                  mock_get_ds_branch, mock_fetch):
        """Returns False because the payload does not indicate a branch change."""
        user = fake_payloads.USER_USERNAME
        namespace = fake_payloads.PROJECT_PATH_WITH_NAMESPACE
        mr_id = 123
        payload = deepcopy(fake_payloads.MR_PAYLOAD)
        msg = mock.Mock(payload=payload)

        # No head pipeline ID
        payload['object_attributes']['head_pipeline_id'] = None
        self.assertIs(ckihook.process_possible_branch_change(user, namespace, mr_id, msg), False)

        # 'merge_status' not in 'changes'.
        payload['object_attributes']['head_pipeline_id'] = fake_payloads.MR_HEAD_PIPELINE_ID
        self.assertIs(ckihook.process_possible_branch_change(user, namespace, mr_id, msg), False)

        mock_fetch.assert_not_called()
        mock_get_ds_branch.assert_not_called()
        mock_cancel.assert_not_called()
        mock_create.assert_not_called()

        # downstream pipeline 'branch' matches mr target branch
        payload['changes']['merge_status'] = {'current': 'unchecked'}
        mock_get_ds_branch.return_value = payload['object_attributes']['target_branch']
        self.assertIs(ckihook.process_possible_branch_change(user, namespace, mr_id, msg), False)
        mock_fetch.assert_called_once_with(user, namespace, mr_id)
        mock_get_ds_branch.assert_called_once_with(msg.gl_instance.return_value,
                                                   mock_fetch.return_value)
        mock_cancel.assert_not_called()
        mock_create.assert_not_called()

        # downstream pipeline 'branch' not found.
        mock_fetch.reset_mock()
        mock_get_ds_branch.reset_mock()
        payload['changes']['merge_status'] = {'current': 'unchecked'}
        mock_get_ds_branch.return_value = None
        self.assertIs(ckihook.process_possible_branch_change(user, namespace, mr_id, msg), False)
        mock_fetch.assert_called_once_with(user, namespace, mr_id)
        mock_get_ds_branch.assert_called_once_with(msg.gl_instance.return_value,
                                                   mock_fetch.return_value)
        mock_cancel.assert_not_called()
        mock_create.assert_not_called()

    @mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=True))
    @mock.patch('webhook.ckihook.fetch_cki')
    @mock.patch('webhook.ckihook.get_downstream_pipeline_branch')
    @mock.patch('webhook.common.cancel_pipeline')
    @mock.patch('webhook.common.create_mr_pipeline')
    def test_process_possible_branch_change_true(self, mock_create, mock_cancel,
                                                 mock_get_ds_branch, mock_fetch):
        """Returns True when a change is detected and pipeline is canceled/triggered."""
        user = fake_payloads.USER_USERNAME
        namespace = fake_payloads.PROJECT_PATH_WITH_NAMESPACE
        mr_id = 123
        payload = deepcopy(fake_payloads.MR_PAYLOAD)
        msg = mock.Mock(payload=payload)
        payload['changes']['merge_status'] = {'current': 'unchecked'}

        # pipeline_branch_matches() says the branches differ so cancel/retrigger and return True
        mock_get_ds_branch.return_value = '8.8'
        self.assertIs(ckihook.process_possible_branch_change(user, namespace, mr_id, msg), True)
        mock_get_ds_branch.assert_called_once_with(msg.gl_instance.return_value,
                                                   mock_fetch.return_value)
        mock_cancel.assert_called_once_with(msg.gl_instance.return_value.projects.get.return_value,
                                            payload['object_attributes']['head_pipeline_id'])
        wow = msg.gl_instance.return_value.projects.get.return_value.mergerequests.get.return_value
        mock_create.assert_called_once_with(wow)

    def test_get_downstream_pipeline_branch(self):
        """Returns the 'branch' pipeline var value, or None."""
        ds_project_id = 23456
        ds_pipeline_id = 87654
        nodes = [{'id': f'gid://gitlab/Ci::Pipeline/{ds_pipeline_id}',
                  'project': {'id': f'gid://gitlab/Project/{ds_project_id}'}
                  }]
        p_data = {'project': {'mr': {'headPipeline': {'downstream': {'nodes': nodes}}}}}
        mock_gl_instance = fakes.FakeGitLab()
        mock_gl_instance.add_project(ds_project_id, 'group/ds_project')
        ds_project = mock_gl_instance.projects.get(ds_project_id)

        # No 'branch' in pipeline vars.
        ds_pipeline_vars = [{'key': 'what', 'value': 'huh'}]
        ds_project.add_pipeline(ds_pipeline_id, variables=ds_pipeline_vars)
        self.assertIs(ckihook.get_downstream_pipeline_branch(mock_gl_instance, p_data), None)

        # A 'branch' in pipeline vars.
        ds_pipeline_vars = [{'key': 'branch', 'value': '8.4'}]
        ds_project.add_pipeline(ds_pipeline_id, variables=ds_pipeline_vars)
        self.assertEqual(ckihook.get_downstream_pipeline_branch(mock_gl_instance, p_data), '8.4')

    @mock.patch('webhook.ckihook.get_instance')
    @mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=True))
    def test_retry_pipelines(self, mock_get_instance):
        """Gets a GL instance and retries their pipelines."""

        mock_failed_pipelines = {123: mock.Mock(project_id=1),
                                 456: mock.Mock(project_id=2),
                                 789: mock.Mock(project_id=2),
                                 321: mock.Mock(project_id=3),
                                 555: mock.Mock(project_id=4)
                                 }

        mock_instance = fakes.FakeGitLab()
        mock_project1 = mock_instance.add_project(1, 'group/project1')
        mock_pipeline10 = mock_project1.add_pipeline(mock_failed_pipelines[123].id)
        mock_job10 = mock_project1.add_job(10, 'prepare', 'success')
        mock_job10.name = 'prepare python'
        mock_pipeline10.jobs.list.return_value = [mock_job10]
        mock_project2 = mock_instance.add_project(2, 'group/project2')
        mock_pipeline20 = mock_project2.add_pipeline(mock_failed_pipelines[456].id)
        mock_job20 = mock_project2.add_job(20, 'prepare', 'success')
        mock_job20.name = 'prepare python'
        mock_pipeline20.jobs.list.return_value = [mock_job20]
        mock_pipeline21 = mock_project2.add_pipeline(mock_failed_pipelines[789].id)
        mock_job21 = mock_project2.add_job(21, 'prepare', 'success')
        mock_job21.name = 'prepare python'
        mock_pipeline21.jobs.list.return_value = [mock_job21]
        mock_project3 = mock_instance.add_project(3, 'group/project3')
        mock_pipeline30 = mock_project3.add_pipeline(mock_failed_pipelines[321].id)
        mock_job30 = mock_project3.add_job(30, 'prepare', 'success')
        mock_job30.name = 'prepare python'
        mock_pipeline30.jobs.list.return_value = [mock_job30]
        mock_project4 = mock_instance.add_project(4, 'group/project4')
        mock_pipeline40 = mock_project4.add_pipeline(mock_failed_pipelines[555].id)
        mock_job40 = mock_project4.add_job(40, 'prepare', 'failed')
        mock_job40.name = 'prepare python'
        mock_pipeline40.jobs.list.return_value = [mock_job40]

        mock_get_instance.return_value = mock_instance
        ckihook.retry_pipelines(mock_failed_pipelines)
        mock_job10.retry.assert_called_once()
        mock_pipeline10.retry.assert_called_once()
        mock_job20.retry.assert_called_once()
        mock_pipeline20.retry.assert_called_once()
        mock_job21.retry.assert_called_once()
        mock_pipeline21.retry.assert_called_once()
        mock_job30.retry.assert_called_once()
        mock_pipeline30.retry.assert_called_once()
        mock_job40.retry.assert_not_called()
        mock_pipeline40.retry.assert_not_called()

    def test_failed_rt_mrs(self):
        """Returns a dict with MR IDs as key and downstream pipeline nodes as values."""
        mock_graphql = mock.Mock()
        namespace = 'group/project'
        branches = ['9.1', '9.1-rt']
        mr1 = {'iid': 1, 'headPipeline': {'downstream': {'nodes': [1, 2, 3]}}}
        mr2 = {'iid': 2, 'headPipeline': {'downstream': {'nodes': [1, 2, 3]}}}
        mock_graphql.client.query.return_value = {'project': {'mrs': {'nodes': [mr1, mr2]}}}
        result = ckihook.failed_rt_mrs(mock_graphql, namespace, branches)
        self.assertEqual(result, {mr1['iid']: mr1['headPipeline']['downstream'],
                                  mr2['iid']: mr2['headPipeline']['downstream']})

    @mock.patch('webhook.ckihook.failed_rt_mrs')
    @mock.patch('webhook.ckihook.map_pipeline_query_results')
    @mock.patch('webhook.ckihook.retry_pipelines')
    def test_retrigger_failed_pipelines(self, mock_retry, mock_map, mock_failed_rt_mrs):
        """Finds failed pipelines and retries them."""
        mock_graphql = mock.Mock()
        namespace = 'group/project'
        branch = mock.Mock(pipelines=[])
        branch.name = '9.0-rt'

        # No realtime pipeline for this branch, nothing to do.
        branch.pipelines = [PipelineType.REGULAR]
        ckihook.retrigger_failed_pipelines(mock_graphql, namespace, branch)
        mock_failed_rt_mrs.assert_not_called()
        mock_retry.assert_not_called()

        # No MRs with failed on merge RT pipelines, nothing to do.
        branch.pipelines = [PipelineType.REALTIME]
        mock_failed_rt_mrs.return_value = {}
        ckihook.retrigger_failed_pipelines(mock_graphql, namespace, branch)
        mock_failed_rt_mrs.assert_called_once()
        mock_retry.assert_not_called()

        # Found some MRs, calls retry_pipelines.\
        mock_failed_rt_mrs.reset_mock()
        mock_pipeline_result1 = mock.Mock(label='CKI_RT::Failed::build')
        mock_pipeline_result2 = mock.Mock(label='CKI_RT::Failed::merge')

        mock_failed_rt_mrs.return_value = {1: {}, 2: {}}
        mock_map.side_effect = [[mock_pipeline_result1], [mock_pipeline_result2]]
        ckihook.retrigger_failed_pipelines(mock_graphql, namespace, branch)
        mock_failed_rt_mrs.assert_called_once()
        mock_retry.assert_called_once_with({2: mock_pipeline_result2})


class TestMRHandler(TestCase):
    """Tests for the MR event handler."""

    @mock.patch('webhook.ckihook.add_labels')
    @mock.patch('webhook.ckihook.cki_label_changed')
    @mock.patch('webhook.ckihook.compute_new_labels')
    @mock.patch('webhook.ckihook.process_possible_branch_change')
    @mock.patch('webhook.ckihook.check_for_missing_labels')
    def test_process_mr_event_label_changed(self, mock_check_for_missing, mock_branch_change,
                                            mock_fetch_new_labels, mock_cki_label_changed,
                                            mock_add_labels):
        """Calls add_labels with the results of cki_label_changed."""
        namespace = fake_payloads.PROJECT_PATH_WITH_NAMESPACE
        mr_id = fake_payloads.MR_IID
        payload = deepcopy(fake_payloads.MR_PAYLOAD)
        payload['changes'] = CHANGES
        projects = mock.Mock()
        branch = projects.get_target_branch.return_value
        msg = mock.Mock(payload=payload)
        mock_branch_change.return_value = False
        mock_graphql = mock.Mock()
        ckihook.process_mr_event(msg, projects, mock_graphql)
        mock_cki_label_changed.assert_called_once_with(payload['changes'])
        mock_branch_change.assert_called_once_with(mock_graphql, namespace, mr_id, msg)
        mock_fetch_new_labels.assert_called_once_with(mock_graphql, namespace, mr_id,
                                                      branch.pipelines)
        mock_add_labels.assert_called_once_with(namespace, mr_id,
                                                mock_fetch_new_labels.return_value)
        mock_check_for_missing.assert_not_called()

    @mock.patch('webhook.ckihook.add_labels')
    @mock.patch('webhook.ckihook.cki_label_changed')
    @mock.patch('webhook.ckihook.compute_new_labels')
    @mock.patch('webhook.ckihook.check_for_missing_labels')
    def test_process_mr_event_rt_waived(self, mock_check_for_missing, mock_compute_new_labels,
                                        mock_cki_label_changed, mock_add_labels):
        """Calls add_labels with the results of cki_label_changed minus CKI_RT."""
        namespace = fake_payloads.PROJECT_PATH_WITH_NAMESPACE
        mr_id = fake_payloads.MR_IID
        payload = deepcopy(fake_payloads.MR_PAYLOAD)
        payload['labels'] = [{'title': 'CKI_RT::Waived'}]
        payload['changes'] = CHANGES
        projects = mock.Mock()
        branch = projects.get_target_branch.return_value
        msg = mock.Mock(payload=payload)
        mock_compute_new_labels.return_value = ['CKI::OK', 'CKI_RT::Failed::merge']
        mock_graphql = mock.Mock()
        ckihook.process_mr_event(msg, projects, mock_graphql)
        mock_cki_label_changed.assert_called_once_with(payload['changes'])
        mock_compute_new_labels.assert_called_once_with(mock_graphql, namespace, mr_id,
                                                        branch.pipelines)
        mock_add_labels.assert_called_once_with(namespace, mr_id, ['CKI::OK'])
        mock_check_for_missing.assert_not_called()

    @mock.patch('webhook.ckihook.add_labels')
    @mock.patch('webhook.ckihook.cki_label_changed')
    @mock.patch('webhook.ckihook.compute_new_labels')
    @mock.patch('webhook.ckihook.check_for_missing_labels')
    def test_process_mr_event_auto_waived(self, mock_check_for_missing, mock_compute_new_labels,
                                          mock_cki_label_changed, mock_add_labels):
        """Calls add_labels with the results of cki_label_changed minus CKI_Automotive."""
        namespace = fake_payloads.PROJECT_PATH_WITH_NAMESPACE
        mr_id = fake_payloads.MR_IID
        payload = deepcopy(fake_payloads.MR_PAYLOAD)
        payload['labels'] = [{'title': 'CKI_Automotive::Waived'}]
        payload['changes'] = CHANGES
        projects = mock.Mock()
        branch = projects.get_target_branch.return_value
        msg = mock.Mock(payload=payload)
        mock_compute_new_labels.return_value = ['CKI::OK',
                                                'CKI_RT::OK',
                                                'CKI_Automotive::Failed::merge']
        mock_graphql = mock.Mock()
        ckihook.process_mr_event(msg, projects, mock_graphql)
        mock_cki_label_changed.assert_called_once_with(payload['changes'])
        mock_compute_new_labels.assert_called_once_with(mock_graphql, namespace, mr_id,
                                                        branch.pipelines)
        mock_add_labels.assert_called_once_with(namespace, mr_id, ['CKI::OK', 'CKI_RT::OK'])
        mock_check_for_missing.assert_not_called()

    @mock.patch('webhook.ckihook.add_labels')
    @mock.patch('webhook.ckihook.cki_label_changed')
    @mock.patch('webhook.ckihook.compute_new_labels')
    @mock.patch('webhook.ckihook.check_for_missing_labels')
    def test_process_mr_event_check_for_missing(self, mock_check_for_missing,
                                                mock_fetch_new_labels, mock_cki_label_changed,
                                                mock_add_labels):
        """No label changes, calls check_for_missing and add_labels."""
        namespace = fake_payloads.PROJECT_PATH_WITH_NAMESPACE
        mr_id = fake_payloads.MR_IID
        payload = deepcopy(fake_payloads.MR_PAYLOAD)
        payload['labels'] = [{'title': 'hey'}]
        projects = mock.Mock()
        branch = projects.get_target_branch.return_value
        msg = mock.Mock(payload=payload)
        mock_cki_label_changed.return_value = []
        mock_graphql = mock.Mock()
        ckihook.process_mr_event(msg, projects, mock_graphql)
        mock_cki_label_changed.assert_called_once_with({})
        mock_fetch_new_labels.assert_called_once_with(mock_graphql, namespace, mr_id,
                                                      branch.pipelines)
        mock_check_for_missing.assert_called_once_with(['hey'], branch.pipelines)
        mock_add_labels.assert_called_once_with(namespace, mr_id,
                                                mock_fetch_new_labels.return_value)

    @mock.patch('webhook.ckihook.add_labels')
    @mock.patch('webhook.ckihook.cki_label_changed')
    @mock.patch('webhook.ckihook.compute_new_labels')
    @mock.patch('webhook.ckihook.check_for_missing_labels')
    def test_process_mr_event_no_action(self, mock_check_for_missing, mock_fetch_new_labels,
                                        mock_cki_label_changed, mock_add_labels):
        """No label changes and no missing CKI labels, nothing to do."""
        payload = deepcopy(fake_payloads.MR_PAYLOAD)
        payload['labels'] = [{'title': 'hey'}]
        projects = mock.Mock()
        branch = projects.get_target_branch.return_value
        msg = mock.Mock(payload=payload)
        mock_cki_label_changed.return_value = []
        mock_check_for_missing.return_value = []
        mock_graphql = mock.Mock()
        ckihook.process_mr_event(msg, projects, mock_graphql)
        mock_cki_label_changed.assert_called_once_with({})
        mock_fetch_new_labels.assert_not_called()
        mock_check_for_missing.assert_called_once_with(['hey'], branch.pipelines)
        mock_add_labels.assert_not_called()

    @mock.patch('webhook.ckihook.add_labels')
    @mock.patch('webhook.ckihook.cki_label_changed')
    @mock.patch('webhook.ckihook.compute_new_labels')
    @mock.patch('webhook.ckihook.process_possible_branch_change')
    @mock.patch('webhook.ckihook.check_for_missing_labels')
    def test_process_mr_event_branch_changed(self, mock_check_for_missing, mock_branch_change,
                                             mock_fetch_new_labels, mock_cki_label_changed,
                                             mock_add_labels):
        """Stops when process_possible_branch_change() returns True."""
        namespace = fake_payloads.PROJECT_PATH_WITH_NAMESPACE
        mr_id = fake_payloads.MR_IID
        payload = deepcopy(fake_payloads.MR_PAYLOAD)
        payload['changes'] = CHANGES
        projects = mock.Mock()
        msg = mock.Mock(payload=payload)
        mock_branch_change.return_value = True
        mock_graphql = mock.Mock()
        ckihook.process_mr_event(msg, projects, mock_graphql)
        mock_branch_change.assert_called_once_with(mock_graphql, namespace, mr_id, msg)
        mock_cki_label_changed.assert_not_called()
        mock_fetch_new_labels.assert_not_called()
        mock_add_labels.assert_not_called()


class TestPipelineHandler(TestCase):
    """Tests for the Pipeline event handler."""

    @mock.patch('webhook.ckihook.PipelineResult', wraps=ckihook.PipelineResult)
    @mock.patch('webhook.ckihook.add_labels')
    def test_process_pipeline_event_no_vars(self, mock_add_labels, mock_PipelineResult):
        """Doesn't do anything."""
        # No mr_url, nothing to do.
        with self.assertLogs('cki.webhook.ckihook', level='INFO') as logs:
            payload = deepcopy(fake_payloads.PIPELINE_PAYLOAD)
            projects = mock.Mock()
            msg = mock.Mock(payload=payload)
            mock_graphql = mock.Mock()
            ckihook.process_pipeline_event(msg, projects, mock_graphql)
            self.assertIn('Event did not contain the expected variables', logs.output[-1])
            mock_add_labels.assert_not_called()
            mock_PipelineResult.assert_not_called()

        # Has mr_url, but retrigger is 'true'
        with self.assertLogs('cki.webhook.ckihook', level='INFO') as logs:
            payload = deepcopy(fake_payloads.PIPELINE_DOWNSTREAM_PAYLOAD)
            payload['object_attributes']['variables'].append(
                {'key': 'CKI_DEPLOYMENT_ENVIRONMENT', 'value': 'retrigger'})
            projects = mock.Mock()
            msg = mock.Mock(payload=payload)
            mock_graphql = mock.Mock()
            ckihook.process_pipeline_event(msg, projects, mock_graphql)
            self.assertIn('Event did not contain the expected variables', logs.output[-1])
            mock_add_labels.assert_not_called()
            mock_PipelineResult.assert_not_called()

    @mock.patch('webhook.ckihook.map_pipeline_query_results')
    @mock.patch('webhook.ckihook.fetch_cki')
    @mock.patch('webhook.ckihook.add_labels')
    def test_process_pipeline_event_sets_label(self, mock_add_labels, mock_FetchCKI,
                                               mock_map_results):
        """Calls FetchCKI and sets a label."""
        with self.assertLogs('cki.webhook.ckihook', level='INFO') as logs:
            namespace = fake_payloads.MR_URL.removeprefix('https://gitlab.com/').split('/-/', 1)[0]
            mr_id = fake_payloads.MR_IID
            payload = deepcopy(fake_payloads.PIPELINE_DOWNSTREAM_PAYLOAD)
            projects = mock.Mock()
            branch = projects.get_target_branch.return_value
            branch.pipelines = [PipelineType.REGULAR]
            msg = mock.Mock(payload=payload)
            mock_PipelineResult = mock.Mock(label='CKI::Running', type=PipelineType.REGULAR)
            mock_PipelineResult.name = 'c9s_merge_request'
            mock_map_results.return_value = [mock_PipelineResult]
            mock_graphql = mock.Mock()
            ckihook.process_pipeline_event(msg, projects, mock_graphql)
            self.assertIn('Setting label CKI::Running', logs.output[-1])
            mock_add_labels.assert_called_once_with(namespace, mr_id, ['CKI::Running'])

    @mock.patch('webhook.ckihook.map_pipeline_query_results')
    @mock.patch('webhook.ckihook.fetch_cki')
    @mock.patch('webhook.ckihook.add_labels')
    def test_process_pipeline_event_failed(self, mock_add_labels, mock_FetchCKI, mock_map_results):
        """Calls PipelineResult and sets a label."""
        # If the event status is Failed get the failed stage and set a failed label.
        with self.assertLogs('cki.webhook.ckihook', level='INFO') as logs:
            namespace = fake_payloads.MR_URL.removeprefix('https://gitlab.com/').split('/-/', 1)[0]
            mr_id = fake_payloads.MR_IID
            payload = deepcopy(fake_payloads.PIPELINE_DOWNSTREAM_PAYLOAD)
            payload['object_attributes']['status'] = 'failed'
            payload['builds'] += [{'stage': 'build', 'status': 'failed'},
                                  {'stage': 'test', 'status': 'skipped'}]
            projects = mock.Mock()
            branch = projects.get_target_branch.return_value
            branch.pipelines = [PipelineType.REGULAR]
            msg = mock.Mock(payload=payload)
            mock_PipelineResult = mock.Mock(label='CKI::Failed::build', type=PipelineType.REGULAR)
            mock_PipelineResult.name = 'c9s_merge_request'
            mock_map_results.return_value = [mock_PipelineResult]
            mock_graphql = mock.Mock()
            ckihook.process_pipeline_event(msg, projects, mock_graphql)
            self.assertIn('Setting label CKI::Failed::build', logs.output[-1])
            mock_add_labels.assert_called_once_with(namespace, mr_id, ['CKI::Failed::build'])

    @mock.patch('webhook.ckihook.map_pipeline_query_results')
    @mock.patch('webhook.ckihook.fetch_cki')
    @mock.patch('webhook.ckihook.add_labels')
    def test_process_pipeline_event_failed_is_running(self, mock_add_labels, mock_FetchCKI,
                                                      mock_map_results):
        """Calls PipelineResult and sets a label."""
        # If the event status is Failed but there are no failed builds then set status to RUNNING.
        with self.assertLogs('cki.webhook.ckihook', level='INFO') as logs:
            namespace = fake_payloads.MR_URL.removeprefix('https://gitlab.com/').split('/-/', 1)[0]
            mr_id = fake_payloads.MR_IID
            payload = deepcopy(fake_payloads.PIPELINE_DOWNSTREAM_PAYLOAD)
            projects = mock.Mock()
            branch = projects.get_target_branch.return_value
            branch.pipelines = [PipelineType.REGULAR]
            msg = mock.Mock(payload=payload)
            mock_PipelineResult = mock.Mock(label='CKI::Running', type=PipelineType.REGULAR)
            mock_PipelineResult.name = 'c9s_merge_request'
            mock_map_results.return_value = [mock_PipelineResult]
            mock_graphql = mock.Mock()
            ckihook.process_pipeline_event(msg, projects, mock_graphql)
            self.assertIn('Setting label CKI::Running', logs.output[-1])
            mock_add_labels.assert_called_once_with(namespace, mr_id, ['CKI::Running'])

    @mock.patch('webhook.ckihook.map_pipeline_query_results')
    @mock.patch('webhook.ckihook.fetch_cki')
    @mock.patch('webhook.ckihook.add_labels')
    def test_process_pipeline_event_no_label(self, mock_add_labels, mock_FetchCKI,
                                             mock_map_results):
        """Calls PipelineResult and does not set a label."""
        # An event for a pipeline that we don't associate with either CKI or CKI_RT labels.
        with self.assertLogs('cki.webhook.ckihook', level='INFO') as logs:
            payload = deepcopy(fake_payloads.PIPELINE_DOWNSTREAM_PAYLOAD)
            payload['object_attributes']['variables'][1]['value'] = 'c9s_rhel9_compat_merge_request'
            projects = mock.Mock()
            branch = projects.get_target_branch.return_value
            branch.pipelines = [PipelineType.REGULAR]
            msg = mock.Mock(payload=payload)
            mock_PipelineResult = mock.Mock(label=None, type=PipelineType.REGULAR)
            mock_PipelineResult.name = 'c9s_rhel9_compat_merge_request'
            mock_map_results.return_value = [mock_PipelineResult]
            mock_graphql = mock.Mock()
            ckihook.process_pipeline_event(msg, projects, mock_graphql)
            self.assertIn('No label to set for this pipeline', logs.output[-1])
            mock_add_labels.assert_not_called()

    @mock.patch('webhook.ckihook.map_pipeline_query_results')
    @mock.patch('webhook.ckihook.fetch_cki')
    @mock.patch('webhook.ckihook.add_labels')
    def test_process_pipeline_event_wrong_type(self, mock_add_labels, mock_FetchCKI,
                                               mock_map_results):
        """Does nothing because the PipelineType is not in the MR Branch."""
        payload = deepcopy(fake_payloads.PIPELINE_DOWNSTREAM_PAYLOAD)
        payload['object_attributes']['variables'][1]['value'] = 'c9s_rhel9_compat_merge_request'
        projects = mock.Mock()
        branch = projects.get_target_branch.return_value
        branch.pipelines = [PipelineType.REGULAR]
        msg = mock.Mock(payload=payload)
        mock_PipelineResult = mock.Mock(label=None, type=PipelineType.REALTIME)
        mock_PipelineResult.name = 'c9s_rhel9_compat_merge_request'
        mock_map_results.return_value = [mock_PipelineResult]
        mock_graphql = mock.Mock()
        with self.assertLogs('cki.webhook.ckihook', level='INFO') as logs:
            ckihook.process_pipeline_event(msg, projects, mock_graphql)
            self.assertIn('Ignoring pipeline event', logs.output[-1])
            mock_add_labels.assert_not_called()


class TestNoteHandler(TestCase):
    """Tests for the Note event handler."""

    @mock.patch('webhook.ckihook.add_labels')
    @mock.patch('webhook.ckihook.compute_new_labels')
    def test_process_note_event(self, mock_fetch_new_labels, mock_add_labels):
        """Runs compute_new_labels and optionally add_labels if there is a proper request."""
        payload = deepcopy(fake_payloads.NOTE_PAYLOAD)
        payload['object_attributes']['note'] = 'request-cki-evaluation'
        projects = mock.Mock()
        branch = projects.get_target_branch.return_value
        mock_msg = mock.Mock(payload=payload)
        mock_graphql = mock.Mock()
        ckihook.process_note_event(mock_msg, projects, mock_graphql)
        mock_fetch_new_labels.assert_called_once_with(mock_graphql,
                                                      payload['project']['path_with_namespace'],
                                                      payload['merge_request']['iid'],
                                                      branch.pipelines)
        mock_add_labels.assert_called_once_with(payload['project']['path_with_namespace'],
                                                payload['merge_request']['iid'],
                                                mock_fetch_new_labels.return_value)

    @mock.patch('webhook.ckihook.add_labels')
    @mock.patch('webhook.ckihook.compute_new_labels')
    def test_process_note_event_bad_note(self, mock_fetch_new_labels, mock_add_labels):
        """Does nothing when force_webhook_evaluation returns False."""
        payload = deepcopy(fake_payloads.NOTE_PAYLOAD)
        payload['object_attributes']['note'] = 'a random comment'
        projects = mock.Mock()
        mock_msg = mock.Mock(payload=payload)
        mock_graphql = mock.Mock()
        with self.assertLogs('cki.webhook.ckihook', level='INFO') as logs:
            ckihook.process_note_event(mock_msg, projects, mock_graphql)
            mock_fetch_new_labels.assert_not_called()
            mock_add_labels.assert_not_called()
            self.assertIn('Note event did not request evaluation', logs.output[-1])

    @mock.patch('webhook.ckihook.add_labels')
    @mock.patch('webhook.ckihook.compute_new_labels')
    def test_process_note_event_no_label_change(self, mock_fetch_new_labels, mock_add_labels):
        """Does nothing when compute_new_labels returns nothing."""
        payload = deepcopy(fake_payloads.NOTE_PAYLOAD)
        payload['object_attributes']['note'] = 'request-cki-evaluation'
        projects = mock.Mock()
        mock_msg = mock.Mock(payload=payload)
        mock_fetch_new_labels.return_value = []
        mock_graphql = mock.Mock()
        with self.assertLogs('cki.webhook.ckihook', level='INFO') as logs:
            ckihook.process_note_event(mock_msg, projects, mock_graphql)
            self.assertIn('No CKI label changes to make', logs.output[-1])


class TestPushHandler(TestCase):
    """Tests for the Push event handler."""

    @mock.patch('webhook.ckihook.retrigger_failed_pipelines')
    def test_process_push_event(self, mock_retrigger):
        """Checks the target_branch for Rt and possibly retriggers pipelines."""
        payload = deepcopy(fake_payloads.PUSH_PAYLOAD)
        mock_msg = mock.Mock(payload=payload)
        mock_projects = mock.Mock()
        mock_graphql = mock.Mock()

        # Branch is not recognized, nothing to do.
        mock_projects.get_target_branch.return_value = None
        ckihook.process_push_event(mock_msg, mock_projects, mock_graphql)
        mock_retrigger.assert_not_called()

        # Branch is not RT, nothing to do.
        mock_branch = mock.Mock()
        mock_branch.name = 'main'
        mock_projects.get_target_branch.return_value = mock_branch
        ckihook.process_push_event(mock_msg, mock_projects, mock_graphql)
        mock_retrigger.assert_not_called()

        # Branch is recognized and RT, away we go...
        mock_branch.name = '9.1-rt'
        mock_projects.get_target_branch.return_value = mock_branch
        ckihook.process_push_event(mock_msg, mock_projects, mock_graphql)
        mock_retrigger.assert_called_once()
