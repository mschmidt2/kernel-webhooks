"""Webhook interaction tests."""
from unittest import TestCase

from webhook import defs
from webhook.pipelines import PipelineType


class TestPipelineType(TestCase):
    """Tests for the PipelineType enum."""

    PIPE_MAP = {'rhel9_private_merge_request': PipelineType.REGULAR,
                'c9s_merge_request': PipelineType.REGULAR,
                'c9s_realtime_merge_request': PipelineType.REALTIME,
                'c9s_rt_merge_request': PipelineType.REALTIME,
                'c9s_automotive_merge_request': PipelineType.AUTOMOTIVE,
                'c9s_rhel9_compat_merge_request': PipelineType.SHADOW,
                'c9s_64k_merge_request': PipelineType._64K
                }

    def _test_pipeline(self, func, pipe_name, result):
        result_string = result.name if isinstance(result, PipelineType) else result
        print(f"Testing {func.__name__}() with '{pipe_name}', expecting '{result_string}'...")
        self.assertIs(func(pipe_name), result)

    def test_pipelinetype_from_str(self):
        """Returns the expected PipelineType value for the given input."""
        func = PipelineType.from_str
        self._test_pipeline(func, '', PipelineType.INVALID)
        self._test_pipeline(func, 'what', PipelineType.INVALID)
        self._test_pipeline(func, 'regular', PipelineType.REGULAR)
        self._test_pipeline(func, 'REALTIME', PipelineType.REALTIME)
        self._test_pipeline(func, 'shadow', PipelineType.SHADOW)
        self._test_pipeline(func, 'Automotive', PipelineType.AUTOMOTIVE)
        self._test_pipeline(func, '64k', PipelineType._64K)
        for pipe_name, pipe_type in self.PIPE_MAP.items():
            self._test_pipeline(func, pipe_name, pipe_type)

    def test_pipeline_prefix(self):
        """Returns the expected CKI label prefix string."""
        self.assertEqual(PipelineType.REGULAR.prefix, defs.CKI_KERNEL_PREFIX)
        self.assertEqual(PipelineType.SHADOW.prefix, defs.CKI_KERNEL_PREFIX)
        self.assertEqual(PipelineType.REALTIME.prefix, defs.CKI_KERNEL_RT_PREFIX)
        self.assertEqual(PipelineType.AUTOMOTIVE.prefix, defs.CKI_KERNEL_AUTOMOTIVE_PREFIX)
        self.assertEqual(PipelineType._64K.prefix, defs.CKI_KERNEL_64K_PREFIX)
        self.assertIs(PipelineType.INVALID.prefix, None)

    def test_friendly_name(self):
        """Returns the name for all types except shadow, in which case returns the regular name."""
        for pipeline in PipelineType:
            if pipeline is PipelineType.SHADOW:
                self.assertEqual(pipeline.friendly_name, PipelineType.REGULAR.name)
            else:
                self.assertEqual(pipeline.friendly_name, pipeline.name)
