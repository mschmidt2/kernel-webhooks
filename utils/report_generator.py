#!/usr/bin/env python
"""Look for MRs that cannot be merged and/or are stale."""

import argparse
from datetime import datetime
from datetime import timedelta
from email.message import EmailMessage
import os
import smtplib

from cki_lib import logger
from cki_lib import misc
import sentry_sdk

from webhook import defs
from webhook.graphql import GitlabGraph

LOGGER = logger.get_logger('utils.report_generator')

KWF_METRICS_URL = ('https://docs.engineering.redhat.com/pages/viewpage.action?'
                   'spaceKey=RHELPLAN&title=KWF+Metrics')

# Get all open MRs.
MR_QUERY = """
query mrData($namespace: ID!, $branches: [String!], $first: Boolean = true, $after: String = "") {
  %s(fullPath: $namespace) {
    id @include(if: $first)
    mergeRequests(state: opened, after: $after, targetBranches: $branches) {
      pageInfo {hasNextPage endCursor}
      nodes {
        iid
        author{username}
        title
        targetBranch
        webUrl
        project{name fullPath}
        updatedAt
        draft
        labels{nodes{title description}}
      }
    }
  }
}
"""


def get_open_mrs(graphql, namespace, namespace_type):
    """Return a list of the MR objects we're interested in."""
    result = graphql.check_query_results(
        graphql.client.query(MR_QUERY % namespace_type,
                             variable_values={'namespace': namespace, 'branches': 'main'},
                             paged_key=f'{namespace_type}/mergeRequests'),
        check_keys={namespace_type})
    mrs = {}
    for mreq in misc.get_nested_key(result, f'{namespace_type}/mergeRequests/nodes'):
        namespace = mreq['project']['fullPath']
        if namespace in mrs:
            mrs[namespace].append(mreq)
            continue
        mrs[namespace] = [mreq]

    LOGGER.debug('%s MRs: found %s', namespace_type.title(), mrs)
    return mrs


def format_mr_entry(mr_data):
    """Format output from mr_data."""
    mr_id = mr_data['iid']
    title = mr_data['title']
    author = mr_data['author']['username']
    mr_url = mr_data['webUrl']
    proj = mr_data['project']['name']
    target_branch = mr_data['targetBranch']

    mr_entry = f"MR {mr_id}: {title} (@{author})\n"
    mr_entry += f"  {mr_url}\n"
    mr_entry += f"  Project: {proj}, Target Branch: {target_branch}\n"

    return mr_entry


def parse_labels(mreq_labels):
    """Get the description text for the labels of interest."""
    label_info = ''
    categories = [defs.READY_FOR_MERGE_LABEL, defs.READY_FOR_QA_LABEL,
                  defs.MERGE_CONFLICT_LABEL, defs.MERGE_WARNING_LABEL]
    matched_categories = []
    interesting_prefixes = ('Acks', 'Bugzilla', 'CKI', 'CKI_RT', 'CommitRefs',
                            'Dependencies', 'JIRA', 'Merge', 'Signoff')

    for label in mreq_labels:
        if label['title'] in categories:
            matched_categories.append(label['title'])
        if label['title'].startswith(interesting_prefixes) and not label['title'].endswith('::OK'):
            label_info += f"  - {label['description']}\n"

    label_info += "\n"

    return label_info, matched_categories


def format_report_body(args, reports):
    """Format the body of the report from the individual section reports."""
    report_body = ''

    if args.conflicts and reports['conflicts']:
        report_body += '---------------------------------\n'
        report_body += 'Cannot be merged to target branch\n'
        report_body += '---------------------------------\n'
        report_body += f"{reports['conflicts']}\n"

    if args.conflicts and reports['warnings']:
        report_body += '-----------------------------------------\n'
        report_body += 'Merge requests conflicting with other MRs\n'
        report_body += '-----------------------------------------\n'
        report_body += f"{reports['warnings']}\n"

    if args.stale:
        if reports['stale_dev']:
            report_body += "---------------------------------------\n"
            report_body += f"MRs stalled in development for {args.stale}+ days\n"
            report_body += "---------------------------------------\n"
            report_body += f"{reports['stale_dev']}\n"
        if reports['stale_qa']:
            report_body += "--------------------------------\n"
            report_body += f"MRs stalled in QA for {args.stale}+ days\n"
            report_body += "--------------------------------\n"
            report_body += f"{reports['stale_qa']}\n"
        if reports['stale_km']:
            report_body += "------------------------------------\n"
            report_body += f"MRs stalled for merge for {args.stale}+ days\n"
            report_body += "------------------------------------\n"
            report_body += f"{reports['stale_km']}\n"
        if reports['stale_draft']:
            report_body += "-------------------------------\n"
            report_body += f"MR drafts stalled for {args.stale}+ days\n"
            report_body += "-------------------------------\n"
            report_body += f"{reports['stale_draft']}\n"

    return report_body


def send_emailed_report(args, body):
    """Create and send an email containing the report output."""
    if not (args.email and args.from_address and args.smtp_url):
        return

    msg = EmailMessage()
    msg['Subject'] = f'Weekly KWF Problematic MR summary ({datetime.utcnow().strftime("%Y.%m.%d")})'
    msg['From'] = args.from_address
    msg['To'] = ', '.join(args.email)
    msg.set_content(body)
    with smtplib.SMTP(args.smtp_url) as smtp:
        smtp.send_message(msg)


def _get_parser_args():
    parser = argparse.ArgumentParser(description='Check open MRs for merge conflicts')

    # Global options
    parser.add_argument('-c', '--conflicts', action='store_true', default=False,
                        help='include MRs with conflicts in the report.')
    parser.add_argument('-g', '--groups', default=os.environ.get('GL_GROUPS', '').split(),
                        help='gitlab groups to fetch MRs from.', nargs='+', required=False)
    parser.add_argument('-p', '--projects', default=os.environ.get('GL_PROJECTS', '').split(),
                        help='gitlab projects to fetch MRs from.', nargs='+', required=False)
    parser.add_argument('-s', '--stale', default=15, type=int, required=False,
                        help='include MRs that have not been updated in X days.')
    parser.add_argument('-e', '--email', default=os.environ.get('EMAIL_TO', '').split(),
                        help='email report to address(es)', nargs='+', required=False)
    parser.add_argument('-f', '--from-address', default=os.environ.get('REPORTER_EMAIL_FROM', ''),
                        help='email report from address', required=False)
    parser.add_argument('-S', '--smtp-url', default=os.environ.get('SMTP_URL', 'localhost'),
                        help='smtp server to use to send report', required=False)
    parser.add_argument('--sentry-ca-certs', default=os.environ.get('REQUESTS_CA_BUNDLE'),
                        help='An optional path to an alternative CA bundle file in PEM-format.')
    return parser.parse_args()


def main(args):
    """Find open MRs, check for merge conflicts and/or stalled MRs."""
    LOGGER.info('Finding open MRs...')
    graphql = GitlabGraph()

    mrs_to_conflict_check = {}
    for group in args.groups:
        mrs_to_conflict_check.update(get_open_mrs(graphql, group, 'group'))
    for project in args.projects:
        mrs_to_conflict_check.update(get_open_mrs(graphql, project, 'project'))

    LOGGER.debug("MR lists: %s", mrs_to_conflict_check)
    if not mrs_to_conflict_check:
        LOGGER.info('No open MRs to process.')
        return

    stale_ts = datetime.utcnow() - timedelta(days=args.stale)

    report_list = ['stale_dev', 'stale_draft', 'stale_qa', 'stale_km', 'conflicts', 'warnings']
    reports = dict.fromkeys(report_list, '')
    for mr_ns, mrs in mrs_to_conflict_check.items():
        for mreq in mrs:
            mr_labels = mreq['labels']['nodes']
            mr_info = format_mr_entry(mreq)
            extra_info = f"  Last Updated: {mreq['updatedAt']}\n"
            (label_info, categories) = parse_labels(mr_labels)
            extra_info += label_info

            if mreq['updatedAt'] < str(stale_ts):
                if defs.READY_FOR_MERGE_LABEL in categories:
                    reports['stale_km'] += mr_info + extra_info
                elif defs.READY_FOR_QA_LABEL in categories:
                    reports['stale_qa'] += mr_info + extra_info
                else:
                    if not mreq['draft']:
                        reports['stale_dev'] += mr_info + extra_info
                    else:
                        reports['stale_draft'] += mr_info + extra_info

            if defs.MERGE_CONFLICT_LABEL in categories and not mreq['draft']:
                reports['conflicts'] += mr_info + "\n"
            elif defs.MERGE_WARNING_LABEL in categories and not mreq['draft']:
                reports['warnings'] += mr_info + "\n"

    report_body = format_report_body(args, reports)

    if report_body:
        report_body += f"Additional info: {KWF_METRICS_URL}"
        send_emailed_report(args, report_body)
    else:
        LOGGER.info("Nothing to report!")

    if not misc.is_production() and report_body:
        LOGGER.info("Report:\n%s\n", report_body)


if __name__ == '__main__':
    args = _get_parser_args()
    misc.sentry_init(sentry_sdk, ca_certs=args.sentry_ca_certs)
    main(args)
