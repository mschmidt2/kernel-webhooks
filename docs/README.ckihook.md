# ckihook Webhook

## Purpose

This webhook handles CKI for internal contributions.

## Manual Runs

You can run the webhook manually on a merge request URL with the command:

    python3 -m webhook.ckihook \
        --disable-inactive-branch-check \
        --merge-request https://gitlab.com/group/repo/-/merge_requests/1 \
        --action pipeline

The [main README](README.md#running-a-webhook-for-one-merge-request) describes
some common environment variables that can be set that are applicable for all
webhooks.

## Reporting

- Label prefix: `CKI::`
- Label prefix: `CKI_RT::`
- Label prefix: `CKI_Automotive::`
- Comment header: N/A

The webhook reports the overall result of the check by applying scoped labels
to the MR with the prefix `CKI::`, `CKI_RT::`, or `CKI_Automotive` An MR may have
one or more of these labels depending on the expected pipeline types for the
target branch of the MR as defined in the utils/rh_metadata.yml file.

The hook does not leave comments in the MR.

## Triggering

To trigger reevaluation of an MR by the ckihook webhook either remove any of
the existing `CKI` scoped labels mentioned above or a leave a comment with one
of the following:

- `request-cki-evaluation`
- `request-ckihook-evaluation`

Alternatively, to retrigger all the webhooks at the same time leave a note in
the MR with `request-evaluation`.
