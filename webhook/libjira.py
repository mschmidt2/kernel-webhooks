"""Library for interacting with jira."""
from datetime import datetime
from os import environ

from cki_lib.logger import get_logger
from cki_lib.misc import is_production
from jira import JIRA

from webhook.defs import GITFORGE
from webhook.defs import JIRA_BOT_ACCOUNTS
from webhook.defs import JIRA_SERVER
from webhook.defs import JIStatus
from webhook.defs import JPFX

LOGGER = get_logger('cki.webhook.libjira')

JIRA_FIELDS = ['assignee', 'components', 'fixVersions', 'issuelinks',
               'labels', 'priority', 'project', 'reporter', 'status',
               'subtasks', 'summary', 'updated', 'versions', 'resolution',
               'customfield_12319940', 'customfield_12321540', 'customfield_12321740']
# customfield_12319940: Target Version field
# customfield_12321540: Preliminary Testing field
# customfield_12321740: Testable Builds field


def connect_jira(token_auth=environ.get('JIRA_TOKEN_AUTH'), host=JIRA_SERVER):
    """Connect to JIRA and return a JIRA connection object."""
    jira_options = {'server': host}
    jiracon = JIRA(options=jira_options, token_auth=token_auth)
    return jiracon


def _clear_issue_cache(issue_cache):
    """Clear the issue_cache list."""
    LOGGER.info('Clearing %s issues from the cache.', len(issue_cache))
    issue_cache.clear()


def _update_issue_cache(new_issues, issue_cache):
    """Put the new_issues in the issue_cache."""
    for issue in new_issues:
        if issue.id in issue_cache:
            LOGGER.warning('JIRA Issue %s already in issue_cache.', issue.key)
        issue_cache[issue.id] = issue


def _getissues(jira, issue_list):
    """Get issue data from JIRA and return a list of Issue objects."""
    LOGGER.info('Fetching JIRA data for these issues: %s', issue_list)
    jql_str = "key="
    # Build jql query string
    jql_str += ' or key='.join(str(issue) for issue in issue_list)
    issues = jira.search_issues(f"{jql_str}", fields=JIRA_FIELDS)
    return issues


def _get_issues_by_id(issue_keys, issue_cache):
    """Return the set of issuess from the cache which match on issue_key."""
    LOGGER.info("issue_keys: %s, issue_cache: %s", issue_keys, issue_cache)
    for issue in issue_cache.values():
        LOGGER.debug("Issue: %s (%s)", issue, issue.id)
        LOGGER.debug("* Status: %s", issue.fields.status)
        LOGGER.debug("* Version(s): %s", [v.name for v in issue.fields.versions])
        LOGGER.debug("* Labels: %s", issue.fields.labels)
        LOGGER.debug("* Component(s): %s", [c.name for c in issue.fields.components])
        LOGGER.debug("* Fix Version(s): %s", [f.name for f in issue.fields.fixVersions])
        testable = None
        if issue.fields.customfield_12321740 is not None:
            testable = issue.fields.customfield_12321740
        LOGGER.debug("* Testable Builds: %s", testable)
        testing = None
        if issue.fields.customfield_12321540 is not None:
            testing = issue.fields.customfield_12321540.value
        LOGGER.debug("* Preliminary Testing: %s", testing)
        tver = None
        if issue.fields.customfield_12319940 is not None:
            tver = issue.fields.customfield_12319940
        LOGGER.debug("* Target Version(s): %s", tver)
    return {issue for issue in issue_cache.values() if issue.key in issue_keys}


def _get_issues_by_alias(alias_list, issue_cache):
    """Return the set of issues from the cache which have an alias which is in alias_list."""
    return {issue for issue in issue_cache.values() for alias in alias_list if alias in issue.alias}


def get_missing_issues(issue_names, issue_list):
    """Return the set of ids that are not in the list of issue objects."""
    missing_keys = {issue_key for issue_key in issue_names if issue_key.startswith(JPFX) and
                    issue_key not in [issue.key for issue in issue_list]}
    missing_aliases = {alias for alias in issue_names if not alias.startswith(JPFX) and
                       alias not in [alias for issue in issue_list for alias in issue.alias]}
    return missing_keys | missing_aliases


def fetch_issues(issue_list, clear_cache=False, issue_cache={}):
    # pylint: disable=dangerous-default-value
    """Return a list of issue objects; taken from the issue_cache dict or queried from JIRA."""
    LOGGER.debug('Cache is %s with %s entries.}', id(issue_cache), len(issue_cache))
    if not issue_list and clear_cache:
        _clear_issue_cache(issue_cache)
        return set()
    if not issue_list:
        LOGGER.debug('Empty input issue_list, nothing to do.')
        return set()

    # Get a JIRA connection object.
    jira = connect_jira()
    if clear_cache:
        _clear_issue_cache(issue_cache)

    # Filter out issues which already exist in the issue_cache
    to_do = get_missing_issues(issue_list, issue_cache.values())
    if to_do != set(issue_list):
        LOGGER.debug('Using issue data from cache: %s', list(set(issue_list) - set(to_do)))
    # Fetch issue data and update the issue_cache with it
    if to_do:
        new_issues = _getissues(jira, to_do)
        LOGGER.info('JIRA issues: %s', new_issues)
        _update_issue_cache(new_issues, issue_cache)
    if missing_issues := get_missing_issues(issue_list, issue_cache.values()):
        LOGGER.warning('JIRA did not return data for these issues: %s', missing_issues)

    issue_ids = [issue_id for issue_id in issue_list if issue_id.startswith(JPFX)]
    aliases = [issue_id for issue_id in issue_list if not issue_id.startswith(JPFX)]
    LOGGER.debug("issue_ids: %s, aliases: %s, cache: %s", issue_ids, aliases, issue_cache)
    return list(_get_issues_by_id(issue_ids, issue_cache) |
                _get_issues_by_alias(aliases, issue_cache))


def issues_with_lower_status(issue_list, status, min_status=JIStatus.NEW):
    """Return the list of issues that have a status lower than the input status."""
    return [issue for issue in issue_list
            if min_status <= JIStatus.from_str(issue.fields.status) < status]


def latest_testing_failed_timestamp(jira, issue):
    """Return most recent timestamp in history when Preliminary Testing field was set to Fail."""
    timestamp = None
    comments = reversed(jira.comments(issue))
    for comment in comments:
        c_detail = jira.comment(issue.id, comment.id)
        if "issued failed testing" in c_detail.body:
            timestamp = datetime.strptime(c_detail.created, '%Y%m%dT%H:%M:%S')
            LOGGER.debug('Preliminary Testing: Fail last added %s', timestamp)
            return timestamp
    LOGGER.warning('Did not find issue failed testing in comment history.')
    return None


def issues_to_move_to_in_progress(issue_list, mr_pipeline_timestamp):
    """Return the issues from the input list that need to be moved to In Progress."""
    # For any input issue with FailedQA we check the timestamp when FailedQA was last added to the
    # issue against the mr_pipeline_timestamp and if the former is newer we pop it from the
    # ji_to_update list.
    # mr_pipeline_timestamp is the datetime when the MR's head pipeline finished, if any.
    ji_to_update = issues_with_lower_status(issue_list, JIStatus.DEVELOPMENT)
    if not (ji_to_check := [issue for issue in ji_to_update
                            if issue.fields.customfield_12321540 is not None
                            and issue.fields.customfield_12321540.value == "Fail"]):
        return ji_to_update

    if not mr_pipeline_timestamp:
        raise ValueError("The MR doesn't have a completed pipeline but an issue has "
                         "Preliminary Testing: Fail?")

    for issue in ji_to_check:
        if failed_qa_timestamp := latest_testing_failed_timestamp(issue_list[0].jira, issue):
            if failed_qa_timestamp > mr_pipeline_timestamp:
                LOGGER.debug('issue %s got Preliminary Testing: Fail after latest pipeline.',
                             issue.key)
                ji_to_update.remove(issue)
    return ji_to_update


def add_gitlab_link_in_issues(issues, this_mr):
    """Add GitLab MR link to Issue Links field in JIRA Issues."""
    base_url = f'{GITFORGE}/{this_mr.namespace}'
    mr_link = f'{base_url}/-/merge_requests/{this_mr.mr_id}'
    title = f'Merge Request: {this_mr.title}'
    icon = (f'{GITFORGE}/assets/favicon-'
            '72a2cad5025aa931d6ea56c3201d1f18e68a8cd39788c7c80d5b2b82aa5143ef.png')
    gitlab_link = {'url': mr_link, 'title': title,
                   'icon': {'url16x16': icon, 'title': 'GitLab Merge Request'}}
    comment = (f"Red Hat's GitLab kernel-webhooks bot linked [a merge request|{mr_link}] to "
               "this issue.\n"
               f"*Title:* {this_mr.title}\n"
               f"*Project:* [{this_mr.project.name}|{base_url}], "
               f"*Target Branch:* [{this_mr.branch.name}|{base_url}/-/tree/{this_mr.branch.name}], "
               f"*Merge Request #:* [{this_mr.mr_id}|{mr_link}].")

    jira = connect_jira()
    for issue in issues:
        current_links = jira.remote_links(issue)
        link_exists = False
        for link in current_links:
            link_detail = jira.remote_link(issue=issue, id=link.id)
            if link_detail.object.url == mr_link:
                link_exists = True
                break
        if link_exists:
            LOGGER.info("MR %s already linked in %s", this_mr.mr_id, issue.key)
        else:
            LOGGER.info("Linking [%s](%s) to issue %s", title, mr_link, issue.key)
            if is_production():
                jira.add_simple_link(issue=issue, object=gitlab_link)
                jira.add_comment(issue.key, comment)


def remove_gitlab_link_comment_in_issue(jira, issue, mr_url):
    """Remove the comment we left in the issue pointing to the MR."""
    for comment in jira.comments(issue):
        c_detail = jira.comment(issue.id, comment.id)
        if mr_url in c_detail.body and c_detail.author.name in JIRA_BOT_ACCOUNTS:
            LOGGER.info("Removing %s comment pointing to %s", issue.key, mr_url)
            c_detail.delete()


def remove_gitlab_link_in_issues(mr_id, namespace, issue_list):
    """Remove GitLab MR link from Issue Links field in JIRA Issues."""
    if not issue_list:
        return

    mr_link = f'{GITFORGE}/{namespace}/-/merge_requests/{mr_id}'
    jira = connect_jira()
    link = {}
    issues = _getissues(jira, issue_list)
    for issue in issues:
        current_links = jira.remote_links(issue)
        link_exists = False
        for link in current_links:
            link_detail = jira.remote_link(issue=issue, id=link.id)
            if link_detail.object.url == mr_link:
                link_exists = True
                break
        if link_exists:
            LOGGER.info("MR %s linked in %s, removing it", mr_id, issue.key)
            jira.delete_remote_link(issue=issue, internal_id=link.id)
            remove_gitlab_link_comment_in_issue(jira, issue, mr_link)


def update_testable_builds(jissues, text, pipeline_url):
    """Fill in Testable Builds field for the given JIRA Issues."""
    issue_list = [jissue.ji for jissue in jissues]
    ji_to_update = issues_with_lower_status(issue_list, JIStatus.READY_FOR_QA, JIStatus.DEVELOPMENT)
    LOGGER.debug("Filling in Testable Builds for issues: %s", ji_to_update)
    LOGGER.debug("Text:\n%s\n", text)
    for issue in ji_to_update:
        if issue.fields.customfield_12321740 and pipeline_url in issue.fields.customfield_12321740:
            LOGGER.info("Nothing to do for issue %s, we already have data", issue.key)
            continue
        if is_production():
            issue.update(fields={'customfield_12321740': text})


def update_issue_status(issue_list, new_status, min_status=JIStatus.NEW):
    """Change the issue status to new_status if it is currently lower, otherwise do nothing."""
    # Do nothing if the current status is lower than min_status.
    # Returns the list of issue objects which have had their status changed.
    if not issue_list:
        LOGGER.info('No issues to update status for.')
        return []
    if new_status not in (JIStatus.DEVELOPMENT, JIStatus.READY_FOR_QA):
        LOGGER.warning("Unsupported transition status: %s", new_status.name)
        return []
    jira = connect_jira()
    if not (ji_to_update := issues_with_lower_status(issue_list, new_status, min_status)):
        LOGGER.info('All issues have status of %s or higher.', new_status.name)
        return []
    ji_keys = [ji.key for ji in ji_to_update]
    LOGGER.info('Updating status to %s for these issues: %s', new_status.name, ji_keys)
    if not is_production():
        for issue in ji_to_update:
            issue.status = new_status.name
        return ji_to_update
    updated = []
    for issue in ji_to_update:
        if new_status is JIStatus.DEVELOPMENT:
            t_comment = f"GitLab kernel MR bot updated status to {new_status.name}"
            jira.transition_issue(issue, new_status, comment=t_comment)
            issue.fields.status = new_status.name
            updated.append(issue)
        if new_status is JIStatus.READY_FOR_QA:
            if issue.fields.customfield_12321540 is None or \
               issue.fields.customfield_12321540.value != 'Pass':
                LOGGER.info("Setting Jira Issue's Preliminary Testing field to Ready")
                issue.update(fields={'customfield_12321540': {'value': 'Ready'}})
    return updated
