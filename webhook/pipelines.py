"""Pipeline helper functions."""
from enum import IntEnum
from enum import auto

from cki_lib import logger

from . import defs

LOGGER = logger.get_logger(__name__)


class PipelineType(IntEnum):
    """Types of Pipelines we might see in our Merge Requests."""

    INVALID = 0
    REGULAR = auto()
    REALTIME = auto()
    AUTOMOTIVE = auto()
    SHADOW = auto()
    _64K = auto()

    @property
    def name(self):
        # pylint: disable=function-redefined,invalid-overridden-method
        """Return the name with any leading underscores (_) removed."""
        return getattr(self, '_name_', '').removeprefix('_')

    @property
    def friendly_name(self):
        """Return a more user-friendly name."""
        # This is just our name unless we are the shadow type then we say regular.
        return self.name if self is not PipelineType.SHADOW else PipelineType.REGULAR.name

    @classmethod
    def from_str(cls, name):
        """Return the PipelineType that corresponds to the given job name."""
        ptype = cls.INVALID
        if not name:
            return ptype
        # Try to simply match the pipe_name to the Type name.
        if ptype := next((ptype for ptype in cls if ptype.name == name.upper()), cls.INVALID):
            return ptype
        if name.endswith("_compat_merge_request"):
            ptype = cls.SHADOW
        elif name.endswith("_merge_request") or name.endswith("_merge_request_private"):
            if "realtime" in name or "_rt_" in name:
                ptype = cls.REALTIME
            elif "automotive" in name:
                ptype = cls.AUTOMOTIVE
            elif "64k" in name:
                ptype = cls._64K
            else:
                ptype = cls.REGULAR
        return ptype

    @property
    def prefix(self):
        """Return the matching CKI label prefix, or None."""
        if self in (PipelineType.REGULAR, PipelineType.SHADOW):
            return defs.CKI_KERNEL_PREFIX
        if self is PipelineType.REALTIME:
            return defs.CKI_KERNEL_RT_PREFIX
        if self is PipelineType.AUTOMOTIVE:
            return defs.CKI_KERNEL_AUTOMOTIVE_PREFIX
        if self is PipelineType._64K:
            return defs.CKI_KERNEL_64K_PREFIX
        return None
